<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "home_tabs".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $brief
 * @property string $icon
 */
class HomeTab extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'home_tabs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'url'], 'required'],
            [['position'], 'integer'],
            [['title', 'url', 'brief', 'icon'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position' => 'Position',
            'title' => 'Title',
            'url' => 'Url',
            'brief' => 'Brief',
            'icon' => 'Icon',
        ];
    }
}
