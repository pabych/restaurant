<?php

namespace common\models;

use himiklab\sitemap\behaviors\SitemapBehavior;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "pages".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $hidden
 * @property integer $position
 * @property string $content
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['hidden', 'position', 'parent_id'], 'integer'],
            [['title', 'url', 'meta_title', 'meta_keywords', 'meta_description', 'og_title', 'og_description'], 'string', 'max' => 255],
            [['og_image'], 'file'],
            [['content'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'url' => 'Url',
            'parent_id' => 'Parent page',
            'meta_title' => 'Meta Title',
            'meta_keywords' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
            'hidden' => 'Not in menu',
            'position' => 'Position',
            'content' => 'Content',
            'og_title' => 'og:title',
            'og_description' => 'og:description',
            'og_image' => 'og:image'
        ];
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'common\behaviors\Slug',
                'in_attribute' => 'title',
                'out_attribute' => 'url',
                'translit' => true
            ],
            'sitemap' => [
                'class' => SitemapBehavior::className(),
                'dataClosure' => function ($model) {
                    /** @var self $model */
                    return [
                        'loc' => Url::to($model->url . '.html', true),
                        'lastmod' => strtotime(date("Y-m-d")),
                        'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                        'priority' => 0.8
                    ];
                }
            ],
        ];
    }

    public function getParent()
    {
        return $this->hasOne(Page::className(), array('id' => 'parent_id'));
    }

    public function getChildren()
    {
        return $this->hasMany(Page::className(), array('parent_id' => 'id'));
    }

    public function getMeta() {
        $metaArray = [];
        $this->meta_title ? $metaArray[] = '<meta name="title" content="' . $this->meta_title . '" />' : null;
        $this->meta_description ? $metaArray[] = '<meta name="description" content="' . $this->meta_description . '" />': null;
        $this->meta_keywords ? $metaArray[] = '<meta name="keywords" content="' . $this->meta_keywords . '" />' : null;
        $this->og_title ? $metaArray[] = '<meta property="og:title" content="' . $this->og_title . '" />' : null;
        $this->og_description ? $metaArray[] = '<meta property="og:description" content="' . $this->og_description . '" />' : null;
        $this->og_image ? $metaArray[] = '<meta property="og:image" content="' . Yii::$app->params['backendUrl'] . '/og_images/' . $this->og_image . '" />' : null;
        return $metaArray;
    }
}
