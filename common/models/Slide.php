<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "slides".
 *
 * @property integer $id
 * @property string $title
 * @property string $brief
 * @property string $image
 */
class Slide extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slides';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title','brief', 'image'], 'required'],
            [['position'], 'integer'],
            [['title', 'brief'], 'string', 'max' => 255],
            [['image'], 'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position' => 'Position',
            'title' => 'Title',
            'brief' => 'Brief',
            'image' => 'Image (recommended size: 860x424)',
        ];
    }
}
