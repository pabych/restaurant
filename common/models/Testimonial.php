<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "testimonials".
 *
 * @property integer $id
 * @property string $author
 * @property string $message
 * @property string $from
 * @property string $email
 * @property string $createdAt
 * @property integer $published
 */
class Testimonial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'testimonials';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author', 'message'], 'required'],
            [['published'], 'integer'],
            [['author', 'from', 'email', 'createdAt'], 'string', 'max' => 255],
            [['message'], 'string']

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author' => 'Author',
            'message' => 'Message',
            'from' => 'From',
            'email' => 'Email',
            'createdAt' => 'Created At',
            'published' => 'Published',
        ];
    }
}
