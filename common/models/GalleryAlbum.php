<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "gallery_albums".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $logo
 *
 * @property AlbumImage[] $albumImages
 */
class GalleryAlbum extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery_albums';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'logo'], 'required'],
            [['title', 'description', 'logo'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'logo' => 'Logo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbumImages()
    {
        return $this->hasMany(AlbumImage::className(), ['album_id' => 'id']);
    }
}
