<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "album_images".
 *
 * @property integer $id
 * @property integer $album_id
 * @property string $description
 * @property string $image
 * @property string $preview_image
 *
 * @property GalleryAlbum $album
 */
class AlbumImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'album_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['album_id', 'image'], 'required'],
            [['album_id'], 'integer'],
            [['image'], 'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'album_id' => 'Album ID',
            'image' => 'Image',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbum()
    {
        return $this->hasOne(GalleryAlbum::className(), ['id' => 'album_id']);
    }
}
