<?php
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\User $user;
 */

?>

<h1><?=$subject ?></h1>
<h2><?=$name ?>, <?=$email ?></h2>
<p><?=$body ?></p>