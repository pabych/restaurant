<?php
return [
    'latitude' => '53.864661',
    'longitude' => '27.539298',
    'supportEmail' => 'pabych93@gmail.com',
    'user.passwordResetTokenExpire' => 3600,
    'frontendUrl' => 'http://restaurant.loc',
    'backendUrl' => 'http://cp.restaurant.loc',
    'telephone' => '+375 29 8889680',
    'fax' => '+375 29 8889680',
    'adminEmail' => 'pabych93@gmail.com',
    'address' => '8901 Marmora Road, Glasgow, D04 89GR.',
    'company_name' => 'company name',
    'company_created_at' => '2013',
    'company_additional' => 'test version',
    'get_way_button' => 'get way',
    'get_way_button_recomendation' => 'need users access',
    'pages' => [
        '' => 'static',
        'contact' => 'contact',
        'gallery' => 'gallery',
        'main' => 'main'
    ],
];
