<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use frontend\assets\MainPageJs;

AppAsset::register($this);
?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!--[if lt IE 8]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0"
                 height="42" width="820"
                 alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
        </a>
    </div>
    <![endif]-->
    <!--[if lt IE 9]>
    <link rel="stylesheet" media="screen" href="css/ie.css">
    <![endif]-->
</head>
<body>
<?php $this->beginBody() ?>
<div class="main">
    <header>
        <div class="container_12">
            <div class="grid_12">
                <h1>
                    <a href="<?=Yii::$app->homeUrl ?>">
                        <img src="<?=Yii::$app->params['frontendUrl'] . '/images/logo-serce2.png' ?>" height="150px" alt="Polski Dom Rodzinny SERCE" />
                    </a>
                </h1>

                <div class="head_top" style="width: 75%;">
                  <div class="socials">
                    <?php foreach(Yii::$app->params['socials'] as $social) : ?>
                      <a href="<?= $social[1] ?>"><i class="<?= $social[0] ?>"></i></a>
                    <?php endforeach; ?>
                  </div>
                  <div class="page1_block">
                    <div class="p1_title center" style="position: absolute;top: 25px;left: 320px;">SERCE</div>
                    <div class="p1_title">Polski Dom Rodzinny</div>
                  </div>
                </div>
                <div class="menu_block">
                    <nav class="horizontal-nav full-width horizontalNav-notprocessed">
                        <ul class="sf-menu">
                            <?php foreach ($pages = \common\models\Page::find()->where(['hidden' => false])->orderBy('position')->all() as $page) : ?>
                                <li class="<?=Yii::$app->request->url == Yii::$app->urlManager->createUrl($page['url'])? 'current': null; ?>">
                                    <a href="<?= Yii::$app->urlManager->createUrl($page['url']) ?>"><?= $page['title'] ?></a>
                                    <?php if(count($page->children) > 0) : ?>
                                        <ul style="display: none;">
                                            <?php foreach($page->children as $childPage) : ?>
                                                <li>
                                                    <a href="<?= Yii::$app->urlManager->createUrl($childPage->url) ?>"><?= $childPage->title ?></a>
                                                    <?php if(count($childPage->children) > 0) : ?>
                                                        <ul style="display: none;">
                                                            <?php foreach($childPage->children as $page) : ?>
                                                                <li>
                                                                    <a href="<?= Yii::$app->urlManager->createUrl($page->url) ?>"><?= $page->title ?></a>
                                                                </li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    <?php endif; ?>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    <?php endif; ?>
                                </li>


                            <?php endforeach; ?>
                        </ul>
                    </nav>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </header>

    <?= $content ?>

</div>
    <footer>
        <div class="container_12">
            <?php foreach ($pages as $page) : ?>
                <div class="grid_2">
                    <h4><a href="<?=Yii::$app->urlManager->createUrl($page['url']) ?>"><?= $page['title'] ?></a></h4>
                </div>
            <?php endforeach; ?>
            <div class="grid_12">
                <div class="copy">
                    <?=Yii::$app->params['company_name'] ?>
                    &copy;
                    <?php if(date("Y") <= Yii::$app->params['company_created_at']) : ?>
                    <?=Yii::$app->params['company_created_at'] ?>
                    <?php else : ?>
                    <?=Yii::$app->params['company_created_at'] . '-' . date("Y") ?>
                    <?php endif; ?>
                  <?php if(Yii::$app->params['company_additional']!=''): ?>
                    &nbsp;|&nbsp;
                    <?=Yii::$app->params['company_additional'] ?>
                  <?php endif; ?>
                </div>
            </div>
        </div>
    </footer>

    <a href="#" id="toTop" style="margin-right: -550px; right: 50%; display: block;"><span id="toTopHover"></span></a>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
