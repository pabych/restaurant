<?php
/* @var $this yii\web\View */
$this->title = 'Testimonial';
isset($page) ? $this->metaTags = $page->getMeta() : null;
?>

<div class="content">
    <div class="container_12">
        <div class="grid_8">
            <div class="box head">
                <h3><?=$testimonial['author'] . ', ' . $testimonial['from'] ?></h3>
            </div>
            <div class="box bx1 tbox">
                <p><?=$testimonial['message'] ?></p>
            </div>
        </div>
        <div class="grid_4">
            <?= Yii::$app->controller->run('banners'); ?>
        </div>
    </div>
</div>