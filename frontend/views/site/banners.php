<?php
/* @var $this yii\web\View */
?>
<?php foreach($banners as $banner) : ?>
    <div class="times">
        <div class="t_title">
            <?=$banner['title'] ?>
        </div>
        <p><?=$banner['content'] ?></p>
    </div>
<?php endforeach; ?>