<?php
use frontend\assets\MainPageJs;
/* @var $this yii\web\View */
$this->title = $page['title'];
$this->metaTags = $page->getMeta();
MainPageJs::register($this);
?>

<?php if($slides) : ?>
<div class="content">
    <div class="container_12">
        <div class="slider_wrapper">
            <div class="grid_12">
                <div class="grid_11 prefix_1 alpha omega">
                    <div id="camera_wrap" class="">
                        <?php foreach($slides as $slide) : ?>
                        <div data-src="<?=Yii::$app->params['backendUrl'] . '/slides/' . $slide['image'] ?>" data-thumb="<?=Yii::$app->params['backendUrl'] . '/slides/' . str_replace('.', '_thumb.', $slide['image']) ?>">
                            <div class="caption fadeIn">
                                <h2><?=$slide['title'] ?><span><?=$slide['brief'] ?></span></h2>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if($page['content']): ?>
  <div class="container_12">
    <div class="grid_12">
      <div class="box bx1">
        <?= $page['content'] ?>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>

<?php if($tabs) : ?>
<div class="page1_block">
    <div class="container_12">
        <?php foreach($tabs as $key => $tab) : ?>
	<?php if($tab['url']=='http://'): continue; endif; ?>
        <div class="grid_6">
            <div class="box maxheight">
                <a href="<?=$tab['url'] ?>" class="icon"><i class="<?=$tab['icon'] ?>"></i></a>
                <div class="p1_title"><?=$tab['title'] ?></div>
                <?=$tab['brief'] ?>
            </div>
        </div>
        <?php
            if((($key+1) % 2) == 0) : ?>
            <div class="clear"></div>
        <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>
<?php endif; ?>

<div class="content page1">
    <div class="container_12">
        <?php if($testimonials = \common\models\Testimonial::find()->where(['published' => true])->all()) : ?>
            <div class="grid_12"> <a href="#" class="next1"></a><a href="#" class="prev1"></a>
                <div class="quote_title">
                    Testimonials
                </div>

                <ul class="carousel2">
                    <?php foreach($testimonials as $testimonial) : ?>
                    <li>
                        <div class="box">„
                            <?php if(strlen($testimonial['message']) > 60) : ?>
                                <?=substr($testimonial['message'], 0, 60) ?>...
                            <?php else : ?>
                                <?=$testimonial['message'] ?>
                            <?php endif; ?>
                            <span><?=$testimonial['author'] . ', ' . $testimonial['from']?></span>
                            <a href="<?=Yii::$app->urlManager->createUrl('testimonial/' . $testimonial['id']) ?>" class="link1">read full story</a>
                            <div class="clear"></div>
                        </div>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>
    </div>
</div>