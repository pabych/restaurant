<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use \frontend\assets\ContactPageJs;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = $page['title'];
$this->metaTags = $page->getMeta();
ContactPageJs::register($this);
?>

<script>
    function getCoordinates() {
        navigator.geolocation.getCurrentPosition(showCoordinates);
    }

    function showCoordinates(position) {
        window.location.href = "<?=Yii::$app->params['frontendUrl'] . '/contact/'?>" + position.coords.latitude + "_" + position.coords.longitude + ".html";
    }
</script>

<div class="content">
    <div class="container_12">
        <div class="grid_4">
            <div class="box head">
                <h3>contact form</h3>
            </div>
            <div class="box bx1">
                <?php $form = ActiveForm::begin(['id' => 'form']); ?>
                <div class="success_wrapper">
                    <div class="success" style="display: block;">
                        <strong><?= Yii::$app->session->getFlash('success'); ?></strong>
                        <strong><?= Yii::$app->session->getFlash('error'); ?></strong>
                    </div>
                </div>
                <div >

                </div>
                <fieldset>
                    <label class="name">
                        <?= $form->field($model, 'name')->label(false)->input('text', ['value' => 'Name: ']) ?>
                        <br class="clear">
                        <span class="error error-empty" style="display: none;">*This is not a valid name.</span>
                        <span class="empty error-empty" style="display: none;">*This field is required.</span>
                    </label>
                    <label class="email">
                        <?= $form->field($model, 'email')->label(false)->input('text', ['value' => 'Email: ']) ?>
                        <br class="clear">
                        <span class="error error-empty" style="display: none;">*This is not a valid email.</span>
                        <span class="empty error-empty" style="display: none;">*This field is required.</span>
                    </label>
                    <label class="subject">
                        <?= $form->field($model, 'subject')->label(false)->input('text', ['value' => 'Subject: ']) ?>
                        <br class="clear">
                        <span class="error error-empty" style="display: none;">*This is not a valid phone number.</span>
                        <span class="empty error-empty" style="display: none;">*This field is required.</span>
                    </label>
                    <label class="message">
                        <?= $form->field($model, 'body')->textArea(['rows' => 6])->label(false)->template = '<textarea name="ContactForm[body]">Message:</textarea>' ?>
                        <br class="clear">
                        <span class="empty error-empty" style="display: none;">*This field is required.</span>
                    </label>

                    <div class="clear"></div>
                    <div class="btns">
                        <a data-type="reset" class="link1">clear</a>
                        <?= Html::a('submit', '#' ,['onclick' => 'document.getElementById("form").submit();','class' => 'link1', 'name' => 'contact-button']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="grid_8">
            <div class="box head">
                <h3>our Address</h3>
            </div>
            <div class="box bx1">
                <div class="map">
                    <figure class="img_inner">
                        <?php if(isset($x) && isset($y)): ?>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m25!1m12!1m3!1d75224.33408668842!2d0!3d0!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m10!1i0!3e6!4m3!3m2!1d<?=$x ?>!2d<?=$y ?>!4m3!3m2!1d<?=$toX ?>!2d<?=$toY ?>!5e0!3m2!1sru!2s!4v1412159354687" width="100%" height="450" frameborder="0" style="border:0"></iframe>
                        <?php else: ?>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3394.228698353218!2d<?=$toY?>!3d<?=$toX ?>!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2s!4v1412258671063" width="100%" height="450" frameborder="0" style="border:0"></iframe>
                        <?php endif; ?>
                        <div>
                            <a href class="link1" onclick="getCoordinates()"><?=Yii::$app->params['get_way_button'] ?></a>
                            <br><div><?=Yii::$app->params['get_way_button_recomendation'] ?></div>
                        </div>
                    </figure>
                    <address>
                        <dl>
                            <dt class="text1">
                                <?=$address ?>
                            </dt>
                            <dd><span>Telephone:</span><?=$telephone ?></dd>
                            <dd><span>FAX:</span><?=$fax ?></dd>
                            <dd>E-mail: <a href="#" class="link-1"><?=$email ?></a></dd>
                        </dl>
                    </address>

                </div>
            </div>
        </div>
    </div>
</div>
