<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<div class="content">
    <div class="container_12">
        <div class="grid_8">
            <div class="box head">
                <h3><?=nl2br(Html::encode($message)) ?></h3>
            </div>
            <div class="box bx1 tbox">
                <p>
                    The above error occurred while the Web server was processing your request.
                </p>
                <p>
                    Please contact us if you think this is a server error. Thank you.
                </p>

            </div>
        </div>
        <div class="grid_4">
            <?= Yii::$app->controller->run('banners'); ?>
        </div>
    </div>
</div>

