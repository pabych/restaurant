<?php
/* @var $this yii\web\View */
$this->title = $page['title'];
$this->metaTags = $page->getMeta();
?>
    <div class="content">
        <div class="container_12">
            <div class="grid_8">
                <div class="box head">
                    <h3><a href="<?=Yii::$app->urlManager->createUrl($page['url']) ?>"><?= $page['title'] ?></a> : <?= $galleryName ?></h3>
                </div>
                <div class="box bx1 tbox">
                    <?= dosamigos\gallery\Gallery::widget(['items' => $items]);?>
                </div>
            </div>
            <div class="grid_4">
                <?= Yii::$app->controller->run('banners'); ?>
            </div>
        </div>
    </div>


