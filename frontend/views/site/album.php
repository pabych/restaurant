<?php

use himiklab\thumbnail\EasyThumbnailImage;
/* @var $this yii\web\View */
$this->title = $page->title;
$this->metaTags = $page->getMeta();
?>
    <div class="content">
        <div class="container_12">
            <div class="grid_8">
                <div class="box head">
                    <h3><?= $this->title ?></h3>
                </div>
                <div class="box bx1 tbox">
                    <?php if(count($albums) > 0) : ?>
                        <?php foreach($albums as $album) : ?>
                            <div class="album">
                                <a href="<?=Yii::$app->getUrlManager()->createUrl('gallery/' . $album->id) ?>">
                                    <div class="album-logo"><img src="<?=Yii::$app->params['backendUrl'] . $album->logo ?>" alt="<?=$album->title?>" title="<?=$album->title?>"/></div>
                                    <div class="album-title"><?=$album->title ?></div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    <?php else : ?>
                        No albums
                    <?php endif; ?>
                </div>
            </div>
            <div class="grid_4">
                <?= Yii::$app->controller->run('banners'); ?>
            </div>
        </div>
    </div>


