<?php
namespace frontend\controllers;

use common\models\AlbumImage;
use common\models\Banner;
use common\models\GalleryAlbum;
use common\models\HomeTab;
use common\models\Page;
use common\models\Slide;
use common\models\Testimonial;
use Faker\Provider\Image;
use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {
        $page = Page::find()->where(['hidden' => false])->orderBy('position')->one();
        return $this->redirect(['site/page', 'url' => $page['url']]);
    }

    public function actionMain()
    {
        $page = Page::findOne(['url' => 'main']);
        $slides = Slide::find()->orderBy('position')->all();
        $testimonials = Testimonial::find()->where(['published' => true])->all();
        $tabs = HomeTab::find()->orderBy('position')->all();

        return $this->render('index', [
            'page' => $page,
            'testimonials' => $testimonials,
            'tabs' => $tabs,
            'slides' => $slides,
        ]);
    }

    public function actionTestimonial($id) {
        $page = Page::findOne(['url' => 'main']);
        $testimonial = Testimonial::findOne(['id' => $id]);
        return $this->render('testimonial', [
            'page' => $page,
            'testimonial' => $testimonial
        ]);
    }

    public function actionAlbum() {
        $page = Page::findOne(['url' => 'gallery']);
        $albums = GalleryAlbum::find()->all();
        return $this->render('album', [
            'page' => $page,
            'albums' => $albums
        ]);
    }

    public function actionGallery($id = null)
    {
        $page = Page::findOne(['url' => 'gallery']);
        $album = GalleryAlbum::findOne($id);
        $items = [];
        /** @var AlbumImage $image */
        foreach ($album->albumImages as $image) {
            $items[] =
                [
                    'url' => Yii::$app->params['backendUrl'] . '/gallery/' . $album->id . '/'.urlencode($image->image),
                    'src' => Yii::$app->params['backendUrl'] . '/gallery/' . $album->id . '/thumb/'.urlencode($image->image),
                    'options' => array('title' => $album->title)
                ];
        }

        return $this->render('gallery', [
            'items' => $items,
            'page' => $page,
            'galleryName' => $album->title
        ]);
    }

    public function actionContact($x = null, $y = null)
    {
        $page =  Page::find()
            ->where('url =:url', ['url' => strtolower(str_replace('action', '', __FUNCTION__))])
            ->one()
        ;

        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['supportEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'page' => $page,
                'x' => $x,
                'y' => $y,
                'toX' => Yii::$app->params['latitude'],
                'toY' => Yii::$app->params['longitude'],
                'address' => Yii::$app->params['address'],
                'telephone' => Yii::$app->params['telephone'],
                'fax' => Yii::$app->params['fax'],
                'email' => Yii::$app->params['adminEmail'],
                'model' => $model,
            ]);
        }
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionPage($url) {
        $page =  Page::find()->where('url =:url', ['url' => $url])->one();
        /*if($this->isActionExist($url)) {
            return $this->redirect(Yii::$app->urlManager->createUrl('/site/' . $url));
        }*/

        if( !$page ) {
            throw new NotFoundHttpException(\Yii::t('app', 'Page not found.'));
        }

        if(isset($page['content'])) {
            $page['content'] = preg_replace('/<img(.*)src=".*\/images(.*)\"/', '<img $1 src="' .
                Yii::$app->params['backendUrl'] . '/images$2"', $page['content']);
        }

        return $this->render('page', ['page' => $page]);
    }

    public function actionBanners() {
        $banners = Banner::find()->where(['published' => true])->orderBy('position')->all();
        return $this->renderPartial('banners', [
            'banners' => $banners
        ]);
    }

    private function isActionExist($url) {
        if(method_exists($this, 'action' . ucfirst($url) )) {
            return true;
        }
        return false;
    }
}
;