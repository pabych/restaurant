<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '.html',
            'rules' => [
                [
                    'pattern' => '/<id:sitemap>',
                    'route' => 'sitemap/default/index',
                    'suffix' => '.xml'
                ],
                '/<action:contact>/<x:[0-9\.]+>_<y:[0-9\.]+>' => 'site/contact',
                '/<action:contact>' => 'site/contact',
                '/<action:gallery>/<id:\w+>' => 'site/gallery',
                '/<action:gallery>' => 'site/album',
                '/<action:main>' => 'site/main',
                '/<action:testimonial>/<id:\d+>' => 'site/testimonial',
                '/<url:[a-zA-Z0-9\-]+>' => 'site/page',

            ]
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
