<?php
return [
    'adminEmail' => 'admin@example.com',
    'socials' => [
        'twitter' => ['icon-twitter', '#'],
        'facebook' => ['icon-facebook', '#'],
        'google+' => ['icon-google-plus', '#'],
    ],
];
