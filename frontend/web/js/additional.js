$(document).ready(function () {
  if($('#camera_wrap').length > 0) {
    jQuery('#camera_wrap').camera({
      loader: false,
      pagination: false,
      thumbnails: true,
      height: '49.30232558139535%',
      caption: false,
      navigation: true,
      fx: 'mosaic'
    });
  }
});

$(window).load(
function () {
  if($('.carousel2').length > 0) {
    $('.carousel2').carouFredSel({
      auto: false, prev: '.prev1', next: '.next1', width: 220, items: {
        visible: {
          min: 1,
          max: 1
        },
        height: 'auto',
        width: '0'

      }, responsive: true,

      scroll: 1,

      mousewheel: false,

      swipe: {onMouse: true, onTouch: true}
    });
  }
});


$(document).ready(function () {
    $().UItoTop({ easingType: 'easeOutQuart' });
});