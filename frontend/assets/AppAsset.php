<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $jsOptions = array(
      'position' => \yii\web\View::POS_HEAD
    );
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/style.css',
        'css/font-awesome.css',
        'css/camera.css',
        'css/form.css',
        'css/ie.css',
    ];
    public $js = [
        'js/jquery.js',
        'js/jquery-migrate-1.1.1.js',
        'js/superfish.js',
        'js/jquery.ui.totop.js',
        'js/jquery.equalheights.js',
        'js/jquery.mobilemenu.js',
        'js/jquery.easing.1.3.js',
        'js/html5shiv.js',
        'js/additional.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
