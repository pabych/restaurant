<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class MainPageJs extends AssetBundle
{
  public $js = [
    'js/camera.js',
    'js/jquery.carouFredSel-6.1.0-packed.js',
    'js/jquery.touchSwipe.min.js',
  ];
}
