<?php
/**
 * Created by PhpStorm.
 * User: pAab
 * Date: 29.12.2014
 * Time: 11:34
 */

namespace frontend\assets;

use yii\web\AssetBundle;

class ContactPageJs extends AssetBundle
{
  public $js = [
    'js/forms.js',
  ];
}
