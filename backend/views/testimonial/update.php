<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Testimonial */

$this->title = 'Update Testimonial: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Testimonials', 'url' => ['testimonial/index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="testimonial-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
