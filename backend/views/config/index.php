<?php

use yii\helpers\Html;
use yii\grid\GridView;


$this->title = 'Home Page';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">
    <?=Yii::$app->controller->run('slider/index'); ?>
    <?=Yii::$app->controller->run('home-tab/index'); ?>
</div>
