<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\InputFile;
use dosamigos\fileupload\FileUpload;
use dosamigos\fileupload\FileUploadUI;

/* @var $this yii\web\View */
/* @var $model common\models\GalleryAlbum */
/* @var $albumImage common\models\AlbumImage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallery-album-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'logo')->widget(InputFile::className(), [
        'controller' => 'elfinder-gallery',
        'filter' => 'image',
        'path' => $model->id ? $model->id : 'logos',
//        'path' => 'logos',
        'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
        'options' => ['class' => 'form-control', 'readonly' => 'readonly'],
        'buttonOptions' => ['class' => 'btn btn-default'],
        'multiple' => false
    ]); ?>

    <div class="form-group field-galleryalbum-logo has-success">
        <label class="control-label" for="galleryalbum-logo">Images</label>

<?php
    $previewImages = [];
    /** @var \common\models\AlbumImage $image */
    foreach($model->albumImages as $image) {
        $previewImages[] = '<div><div onclick="removeImg('.$image->id.',$(this))" class="remove-button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></div>' . Html::img(Yii::$app->params['backendUrl'] . '/gallery/' . $model->id . '/thumb/' . $image->image, ['class' => 'file-preview-image', 'alt' => '']) . '</div>';
    } ?>
    <?= \kartik\file\FileInput::widget([
        'name' => 'images[]',
        'options' => ['multiple' => true, 'accept' => 'image/*'],
        'pluginOptions' => [
            'previewFileType' => 'image',
            'showUpload' => false,
            'initialPreview' => $previewImages,
            'overwriteInitial'=>false,
            'multiple' => true
        ]
    ]); ?>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>


<script>
    function removeImg(id, itself) {
        $.ajax({
            type: "POST",
            url: "<?=Yii::$app->getUrlManager()->createUrl('gallery-album/remove') ?>",
            data: {
                id : id
            },
            dataType: "html",
            success: function () {
                $(itself[0]).parent().parent().hide();
            }
        });
    }
</script>