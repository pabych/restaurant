<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\GalleryAlbumSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gallery Albums';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-album-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Gallery Album', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            'description',
            'logo' => [
                'label' => 'Logo',
                'format' => 'html',
                'contentOptions'=>['style'=>'width: 100px;'],
                'value' =>
                    function ($data) {
                        return Html::img($data->logo, ['style' => 'height:100px; max-width:500px']);
                    }
            ],
            [
                'class' => \yii\grid\ActionColumn::className(),
                'buttons'=>[
                    'view'=>function ($url, $model) {
                        $customurl=Yii::$app->getUrlManager()->createUrl(['log/view','id'=>$model['id']]); //$model->id для AR
                        return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl,
                            ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                    }
                ],
                'template'=>'{update}  {delete}',
            ]
        ],
    ]); ?>

</div>
