<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>
<!--<script type="text/javascript" src="<your installation path>/tinymce/tinymce.min.js"></script>-->
<!--<script type="text/javascript">-->

<div class="page-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'position')->input('number') ?>

    <?php
    $pages = \common\models\Page::find()->where('id != :own', ['own' => $model->id])->all();
    $pagesList = [];
    $pagesList[null] = '<no parent>';
    foreach($pages as $page) {
        $pagesList[$page->id] = $page->title;
    }
    ?>

    <?=Html::listBox('Page[parent_id]', $model->parent ? $model->parent->id : '', $pagesList, [
        'size' => count($pages) + 1,
        'style' => 'overflow:auto',
        'class' => 'form-control'
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'url', [])->listBox(Yii::$app->params['pages']) ?>

    <?= $form->field($model, 'hidden')->checkbox() ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#meta" class="collapsed">
                    META
                </a>
            </h4>
        </div>
        <div id="meta" class="panel-collapse collapse" style="height: 0px;">
            <div class="panel-body">
                <?= $form->field($model, 'meta_title')->textInput(['maxlength' => 255]) ?>
                <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => 255]) ?>
                <?= $form->field($model, 'meta_description')->textInput(['maxlength' => 255]) ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#og-meta" class="collapsed">
                    Open Graph META
                </a>
            </h4>
        </div>
        <div id="og-meta" class="panel-collapse collapse" style="height: 0px;">
            <div class="panel-body">
                <?= $form->field($model, 'og_description')->textInput() ?>
                <?= $form->field($model, 'og_title')->textInput(['style' => 'btn']) ?>
                <?= $form->field($model, 'og_image')->fileInput() ?>
            </div>
        </div>
    </div>


    <?php
    echo $form->field($model, 'content')->widget(CKEditor::className(),[
        'editorOptions' => ElFinder::ckeditorOptions('elfinder',[
                'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                'inline' => false, //по умолчанию false
            ]),
    ]);
    ?>





    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
