<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Page', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'url',
            'parent' => [
                'label' => 'Parent',
                'value' => function($data) {
                    return $data->parent == null ? '<no parent>' : $data->parent->title;
                }
            ],
            'hidden' => [
                'label' => 'Not in menu',
                'format' => 'html',
                'value' =>
                    function($data) {
                        return $data->hidden ? 'true' : 'false' ;
                    },
            ],
            'position',

            [
                'class' => \yii\grid\ActionColumn::className(),
                'buttons'=>[
                    'view'=>function ($url, $model) {
                            $customurl=Yii::$app->getUrlManager()->createUrl(['log/view','id'=>$model['id']]); //$model->id для AR
                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl,
                                ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                        }
                ],
                'template'=>'{update}  {delete}',
            ]
        ],
    ]); ?>
</div>
