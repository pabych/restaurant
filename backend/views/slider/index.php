<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SlideSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="slide-index">

    <h2>Slides</h2>
    <p>
        <?= Html::a('Add Slide', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'position',
            'title',
            'brief' => [
                'label' => 'Brief',
                'attribute' => 'brief',
            ],
            'image' => [
                'label' => 'Image',
                'format' => 'html',
                'value' =>
                    function($data) {
                        return Html::img(Yii::$app->params['backendUrl'] . '/slides/' . str_replace('.', '_thumb.', $data->image));
                    },
            ],

            [
                'class' => \yii\grid\ActionColumn::className(),
                'buttons'=>[
                    'view'=>function ($url, $model) {
                            $customurl=Yii::$app->getUrlManager()->createUrl(['log/view','id'=>$model['id']]); //$model->id для AR
                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl,
                                ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                        }
                ],
                'template'=>'{update}  {delete}',
            ]
        ],
    ]); ?>

</div>
