<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HomeTabSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="home-tab-index">

    <h2>Block</h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Add Block', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'position',
            'title',
            'url:url',
            'brief',
            'icon',

            [
                'class' => \yii\grid\ActionColumn::className(),
                'buttons'=>[
                    'view'=>function ($url, $model) {
                            $customurl=Yii::$app->getUrlManager()->createUrl(['log/view','id'=>$model['id']]); //$model->id для AR
                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl,
                                ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                        }
                ],
                'template'=>'{update}  {delete}',
            ]
        ],
    ]); ?>

</div>
