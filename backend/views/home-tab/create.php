<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HomeTab */

$this->title = 'Create Block';
$this->params['breadcrumbs'][] = ['label' => 'Configuration', 'url' => ['config/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-tab-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
