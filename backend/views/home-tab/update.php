<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HomeTab */

$this->title = 'Update Block: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Configuration', 'url' => ['config/index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="home-tab-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
