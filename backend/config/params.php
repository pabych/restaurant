<?php
return [
    'adminEmail' => 'admin@example.com',
    'icons' => [
        'icon-book' => 'book',
        'icon-map-marker' => 'map-marker',
        'icon-heart' => 'heart',
        'icon-microphone' => 'microphone',
    ],
];
