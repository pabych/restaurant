<?php

namespace backend\controllers;

use Yii;
use common\models\Slide;
use app\models\SlideSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * HomeController implements the CRUD actions for Slide model.
 */
class SliderController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Slide models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SlideSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderPartial('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Slide model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Slide model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Slide();

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isPost) {
                $model->image = UploadedFile::getInstance($model, 'image');

                if ($model->validate()) {
                    $fileName = 'slides/' . $model->image->baseName . '.' . $model->image->extension;
                    $model->image->saveAs($fileName);
                    list($width, $height) = getimagesize($fileName);
                    $new_width = 130;
                    $new_height = 68;
                    $image_p = imagecreatetruecolor($new_width, $new_height);
                    $image = imagecreatefromjpeg($fileName);
                    imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                    imagejpeg($image_p, 'slides/' . $model->image->baseName . '_thumb.' . $model->image->extension, 100);
                }
                $model->save();
            }
            return $this->redirect(['/config/index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Slide model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isPost) {
                $model->image = UploadedFile::getInstance($model, 'image');

                if ($model->validate()) {
                    $fileName = 'slides/' . $model->image->baseName . '.' . $model->image->extension;
                    $model->image->saveAs($fileName);
                    list($width, $height) = getimagesize($fileName);
                    $new_width = 130;
                    $new_height = 68;
                    $image_p = imagecreatetruecolor($new_width, $new_height);
                    $image = imagecreatefromjpeg($fileName);
                    imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                    imagejpeg($image_p, 'slides/' . $model->image->baseName . '_thumb.' . $model->image->extension, 100);
                }
                $model->save();
            }
            return $this->redirect(['/config/index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Slide model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['/config/index']);
    }

    /**
     * Finds the Slide model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Slide the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Slide::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
