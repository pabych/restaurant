<?php

namespace backend\controllers;

use common\models\AlbumImage;
use Yii;
use common\models\GalleryAlbum;
use backend\models\GalleryAlbumSearch;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * GalleryAlbumController implements the CRUD actions for GalleryAlbum model.
 */
class GalleryAlbumController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all GalleryAlbum models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GalleryAlbumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new GalleryAlbum model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GalleryAlbum();
        $albumImage = new AlbumImage();

        if ($_POST) {
            $model->title = $_POST['GalleryAlbum']['title'];
            $model->description = $_POST['GalleryAlbum']['description'];
            $model->logo = $_POST['GalleryAlbum']['logo'];
            if($model->validate()) {
                $model->save();
                if(!file_exists('gallery/' . $model->id)) {
                    mkdir('gallery/' . $model->id, 0777, true);
                }
                $imageInstances = UploadedFile::getInstancesByName('images');

                /** @var UploadedFile $imageInstance */
                foreach ($imageInstances as $imageInstance) {
//                    $this->createThumbImage($imageInstance);
                    $albumImage = new AlbumImage();
                    $albumImage->album_id = $model->id;
                    $albumImage->image = $imageInstance;
                    $albumImage->image
                        ->saveAs('gallery/' . $model->id . '/' . $imageInstance->getBaseName() . '.' . $imageInstance->getExtension());
                    $this->createThumbImage($imageInstance->name, $model->id);
                    $albumImage->save();
                }
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'albumImage' => $albumImage
            ]);
        }
    }

    /**
     * Updates an existing GalleryAlbum model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($_POST) {
            $model->title = $_POST['GalleryAlbum']['title'];
            $model->description = $_POST['GalleryAlbum']['description'];
            $model->logo = $_POST['GalleryAlbum']['logo'];
            if($model->validate()) {
                $model->save();
                if(!file_exists('gallery/' . $model->id)) {
                    mkdir('gallery/' . $model->id, 0777, true);
                }
                $imageInstances = UploadedFile::getInstancesByName('images');

                /** @var UploadedFile $imageInstance */
                foreach ($imageInstances as $imageInstance) {
                    $albumImage = new AlbumImage();
                    $albumImage->album_id = $model->id;
                    $albumImage->image = $imageInstance;
                    $albumImage->image
                        ->saveAs('gallery/' . $model->id . '/' . $imageInstance->getBaseName() . '.' . $imageInstance->getExtension());
                    $this->createThumbImage($imageInstance->name, $model->id);
                    $albumImage->save();
                }
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing GalleryAlbum model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        foreach($this->findModel($id)->albumImages as $image) {
            if($this->deleteDirectory('gallery/' . $id)) {
                $image->delete();
            }
        }
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    public function actionRemove() {
        if($id = $_POST['id']) {
            $album = AlbumImage::findOne($id);
            file_exists('gallery/' . $album->album_id . '/' . $album->image) ? unlink('gallery/' . $album->album_id .
                '/' . $album->image) : null;
            file_exists('gallery/' . $album->album_id . '/thumb/' . $album->image) ? unlink('gallery/' . $album->album_id .
                '/thumb/' . $album->image) : null;
            $album->delete();
            return true;
        }
        return false;
    }

    /**
     * Finds the GalleryAlbum model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GalleryAlbum the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GalleryAlbum::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $fileName
     * @param $albumId
     */
    private function createThumbImage($fileName, $albumId) {
        $filePath = 'gallery/' . $albumId . '/' . $fileName;
        list($width, $height) = getimagesize('gallery/' . $albumId . '/' . $fileName);
        $newHeight = 150;
        $ratio = $newHeight / $height;
        $newWidth = $width * $ratio;
        $image_p = imagecreatetruecolor($newWidth, $newHeight);
        $image = imagecreatefromjpeg($filePath);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
        if(!file_exists('gallery/' . $albumId . '/thumb/')) {
            mkdir('gallery/' . $albumId . '/thumb/', 0777, true);
        }
        imagejpeg($image_p, 'gallery/' . $albumId . '/thumb/' . $fileName, 100);
    }

    /**
     * @param $dir
     * @return bool
     */
    private function deleteDirectory($dir) {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }

        }

        return rmdir($dir);
    }
}
