<?php

use yii\db\Schema;
use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

//
//         $this->createTable('pages', array(
//            'id' => 'int(11) AUTO_INCREMENT NOT NULL PRIMARY KEY',
//            'title' => 'varchar(255) NOT NULL',
//            'url' => 'varchar(255) NOT NULL',
//            'meta_title' => 'varchar(255)',
//            'meta_keywords' => 'varchar(255)',
//            'meta_description' => 'varchar(255)',
//            'parent_id' => 'int(11)',
//            'hidden' => 'boolean',
//            'position' => 'int(11)',
//            'content' => 'text',
//            'og_title' => 'varchar(255)',
//            'og_description' => 'varchar(255)',
//            'og_image' => 'varchar(255)'
//        ));
//        $this->createIndex('parent_id', 'pages', 'parent_id');
//        $this->addForeignKey('parent_id', 'pages', 'parent_id', 'pages', 'id');


        $this->createTable('gallery_albums', array(
            'id' => 'int(11) AUTO_INCREMENT NOT NULL PRIMARY KEY',
            'title' => 'varchar(255) NOT NULL',
            'description' => 'varchar(255)',
            'logo' => 'varchar(255)',
        ));
//
        $this->createTable('album_images', array(
            'id' => 'int(11) AUTO_INCREMENT NOT NULL PRIMARY KEY',
            'album_id' => 'int(11) NOT NULL',
            'image' => 'longblob',
        ));

        $this->addForeignKey('albums_images', 'album_images', 'album_id', 'gallery_albums', 'id');
//        $this->createTable('{{%user}}', [
//            'id' => Schema::TYPE_PK,
//            'username' => Schema::TYPE_STRING . ' NOT NULL',
//            'auth_key' => Schema::TYPE_STRING . '(32) NOT NULL',
//            'password_hash' => Schema::TYPE_STRING . ' NOT NULL',
//            'password_reset_token' => Schema::TYPE_STRING,
//            'email' => Schema::TYPE_STRING . ' NOT NULL',
//            'role' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 10',
//
//            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 10',
//            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
//            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
//        ], $tableOptions);
//
//        $this->insert('user', [
//            'id' => NULL,
//            'username' => 'admin',
//            'auth_key' => '5Z3vbUoD5Xt5_4PNNOwSsYRq3Pq4A0SZ',
//            'password_hash' => '$2y$13$zHQtALfRkFzFbEIXbhEHF.ydx.5B02Q7J3WzuuaoCmWYiuAkdDo6q',
//            'password_reset_token' => NULL,
//            'email' => 'pabych93@gmail.com',
//            'role' => '10',
//            'status' => '10',
//            'created_at' => '1412063803',
//            'updated_at' => '1412063803',
//            ]
//        );
//
//        $this->createTable('slides', array(
//            'id' => 'int(11) AUTO_INCREMENT NOT NULL PRIMARY KEY',
//            'position' => 'int(11)',
//            'title' => 'varchar(255) NOT NULL',
//            'brief' => 'varchar(255)',
//            'image' => 'longblob',
//        ));
//
//        $this->createTable('home_tabs', [
//            'id' => 'int(11) AUTO_INCREMENT NOT NULL PRIMARY KEY',
//            'position' => 'int(11)',
//            'title' => 'varchar(255) NOT NULL',
//            'url' => 'varchar(255) NOT NULL',
//            'brief' => 'varchar(255)',
//            'icon' => 'varchar(255)',
//        ]);
//
//        $this->createTable('testimonials', [
//            'id' => 'int(11) AUTO_INCREMENT NOT NULL PRIMARY KEY',
//            'position' => 'int(11)',
//            'author' => 'varchar(255) NOT NULL',
//            'message' => 'text NOT NULL',
//            'from' => 'varchar(255)',
//            'email' => 'varchar(255)',
//            'createdAt' => 'varchar(255)',
//            'published' => 'boolean',
//        ]);
//        $this->createTable('banners', [
//            'id' => 'int(11) AUTO_INCREMENT NOT NULL PRIMARY KEY',
//            'position' => 'int(11)',
//            'title' => 'varchar(255) NOT NULL',
//            'content' => 'text NOT NULL',
//            'published' => 'boolean',
//        ]);
    }

    public function down()
    {
//        $this->dropForeignKey('albums_images', 'album_images');
//        $this->dropTable('gallery_albums');
//        $this->dropTable('album_images');
//        $this->dropTable('{{%user}}');
//        $this->dropTable('pages');
//        $this->dropTable('slides');
//        $this->dropTable('home_tabs');
//        $this->dropTable('testimonials');
//        $this->dropTable('banners');
    }
}
